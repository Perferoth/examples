<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class ModuleAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permitId = null)
    {
        if ($this->hasUserPermit($request, $permitId) || $this->hasRolePermit($request, $permitId)) {
            return $next($request);
        } else {
            $msgAdmin = 'Nie posiadasz uprawnień dla danego modułu!';
            return redirect()->to('office')->with([
                'msgAdmin' => $msgAdmin,
                'status' => 'danger'
            ]);
        }
    }

    private function hasRolePermit($request, $permitId)
    {
        $roles = $this->getUserRoles($request);

        foreach ($roles as $roleId => $roleInfo) {
            if (in_array($permitId, Role::find($roleId)->permits()->getRelatedIds()->toArray())) {
                return true;
            }
        }

        return false;
    }

    private function hasUserPermit($request, $permitId)
    {
        $userPermits = $request->user()->permits()->getRelatedIds()->toArray();

        if (in_array($permitId, $userPermits)) {
            return true;
        } else {
            return false;
        }
    }

    private function getUserRoles($request)
    {
        foreach ($request->user()->roles as $role) {
            $userRoles[$role->id] = $role->info;
        }

        return $userRoles;
    }
}
