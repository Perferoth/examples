<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class File extends Model
{
    //use SoftDeletes;

    protected $fillable = ['name','path','title','alt','type','size','visible'];

    public $baseDir = 'upload/files/';

    public function file()
    {
        return $this->belongsTo(' App\Http\Controllers\Files');
    }

    public static function saveFile($data, $setTitle, $setAlt)
    {
        $file = new static;

        $file->pathPrefix = date('Y_m_d');
        $file->filePrefix = time();
        $file->generatedName = $file->filePrefix . '_' . $file->parseFileName($data);
        $file->uploadPath = $file->baseDir . $file->pathPrefix .'/'. $data->getMimeType();

        $file->id = $file->create([
            'name' => $file->generatedName,
            'path' => $file->uploadPath,
            'title' => $setTitle,
            'alt' => $setAlt,
            'type' => $data->getMimeType(),
            'size' => $data->getSize(),
            'visible' => 1,
            'created_at' => getdate(),
            'updated_at' => NULL
        ])->id;

        return $file;
    }

    private function parseFileName($file)
    {
        $fileInfo = pathinfo($file->getClientOriginalName());

        // Slugify name
        $slugifiedFileName = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $fileInfo['filename'])));

        // Re-add file extension
        $parsedFileName = $slugifiedFileName .'.'. $fileInfo['extension'];

        return $parsedFileName;
    }
}
