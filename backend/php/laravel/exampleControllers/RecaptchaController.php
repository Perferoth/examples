<?php

namespace App\Http\Controllers\Google;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecaptchaController extends Controller
{
    public $siteKey = 'yourSiteKey';
    protected $secretKey = 'yourSecretKey';
    protected $url = 'https://www.google.com/recaptcha/api/siteverify';
    protected $token;
    private $minScore = 0.5;

    public function checkResponse($gToken)
    {
        $this->token = $gToken;

        $data = array(
            'secret' => $this->secretKey,
            'response' => $this->token
        );
        $options = array(
            'http' => array (
                'header' => 'Content-Type: application/x-www-form-urlencoded\r\n',
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $response = json_decode(file_get_contents($this->url, false, $context));


        if($response->success && $response->score >= $this->minScore) {
            $response = true;
        } else{
            $response = false;
        }

        return $response;
    }
}
