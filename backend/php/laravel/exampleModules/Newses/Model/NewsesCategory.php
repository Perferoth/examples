<?php

namespace App\Modules\Newses\Model;

use Illuminate\Database\Eloquent\Model;

class NewsesCategory extends Model
{

    protected $table = 'newses_categories';
    protected $fillable = ['title','description','author'];

    /**
     * Get the newses associated with the given categories.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function newses()
    {
        return $this->belongsToMany('App\Modules\Newses\Model\News','category_news','category_id','news_id');
    }
}
