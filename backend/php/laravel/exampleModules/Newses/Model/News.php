<?php

namespace App\Modules\Newses\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class News extends Model
{
    use SoftDeletes;
	// Specify table name
	protected $table = 'newses';
	protected $fillable = ['title','description','content','author','album_id'];
    protected $dates = ['deleted_at'];

    /**
     * Get the categories associated with given News.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function categories()
    {
        return $this->belongsToMany('App\Modules\Newses\Model\NewsesCategory', 'category_news', 'news_id','category_id');
    }

	
}
