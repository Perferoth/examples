<?php
/**
 * @Date: 18.11.16 18:55
 * @Author: Piotr Ferfecki
 * @Company: FER-TECH Piotr Ferfecki
 *
 * @Function: Newses Controller
 */

namespace App\Modules\Newses\Controller\Admin;

use App\Modules\Galleries\Model\Album;
use App\Modules\Newses\Model\NewsesCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Modules\Newses\Model\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Modules\Newses\Controller\Admin\NewsesCategoriesController;
use Theme;


class NewsesController extends Controller
{
    /**
     * Listing all newses in admin panel.
     * ---
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listNewses()
    {
    	$newses = News::all();

    	return view(Theme::path()->admin .'.modules.newses.list', compact('newses'));
    }

    /**
     * Responsible for create/edit formular of newses.
     * ---
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formNews($id = null)
    {
        if($id == NULL){
            $action = 'create';
        } else {
            $news = News::find($id);
            $action = 'update';
        }

        $categories = NewsesCategory::all();
        $albums = Album::all();

    	return view(Theme::path()->admin .'.modules.newses.form', compact('news','action','id','categories','albums'));
    }

    /**
     *  Responsible for saving newses.
     *  ---
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createNews(Request $request)
    {
    	$news = News::create($request->all() +
    		[
    			'author' => Auth::user()->name
    		]);

        $this->bindCategory($request->categories,$news);

    	return redirect()->to('office/newses');
    }

    /**
     *  Responsible for updating existing newses.
     *  ---
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateNews(Request $request, $id)
    {

        News::find($id)->update($request->all() +
            [
                'author' => Auth::user()->name
            ]);


        $this->bindCategory($request->categories, News::find($id));


        return redirect()->to('office/newses');
    }

    /**
     * Responsible for deleting newses.
     * ---
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteNews($id)
    {
        News::find($id)->delete();

        return redirect()->to('office/newses');
    }

    private function bindCategory($categoriesId , News $news)
    {
            $news->categories()->sync((array) $categoriesId);
    }



}

