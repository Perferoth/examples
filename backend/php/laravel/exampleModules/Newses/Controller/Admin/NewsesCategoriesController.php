<?php

namespace App\Modules\Newses\Controller\Admin;

use App\Modules\Newses\Model\NewsesCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Modules\Newses\Model\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Theme;


class NewsesCategoriesController extends Controller
{
    public function listCategories()
    {
    	$categories = NewsesCategory::all();

    	return view(Theme::path()->admin .'.modules.newses.categories.list', compact('categories'));
    }

    public function formCategory($id = null)
    {
        if($id == NULL){
            $action = 'create';
        } else {
            $category = NewsesCategory::find($id);
            $action = 'update';
        }

    	return view(Theme::path()->admin .'.modules.newses.categories.form', compact('category','action','id'));
    }

    public function createCategory(Request $request)
    {
    	NewsesCategory::create($request->all() +
    		[
    			'author' => Auth::user()->name
    		]);

    	$msgAdmin = 'Kategoria została dodana!';
    	return redirect()->to('office/newses/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);
    }

    public function updateCategory(Request $request,$id)
    {
        NewsesCategory::find($id)->update($request->all() +
            [
                'author' => Auth::user()->name
            ]);

        $msgAdmin = 'Kategoria została zaktualizowana!';
        return redirect()->to('office/newses/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);

    }

    public function deleteCategory($id){

        NewsesCategory::find($id)->delete();

        $msgAdmin = 'Kategoria została usunięta!';
        return redirect()->to('office/newses/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);

    }

}

