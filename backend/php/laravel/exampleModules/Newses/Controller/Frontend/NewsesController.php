<?php

namespace App\Modules\Newses\Controller\Frontend;

use App\Modules\Galleries\Model\Album;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;



use App\Http\Requests;
use App\Modules\Newses\Model\News;
use App\Modules\Newses\Model\NewsesCategory;
use App\Http\Controllers\Controller;
use Theme;

class NewsesController extends Controller
{
    public function showNewses()
    {
    	$query = News::query();

        $categories = $this->getAllCategories();

    	if(Request::input('category'))
        {
            $choosedCategory = Request::input('category');
            $query->whereHas('categories', function ($q) use ($choosedCategory) {
                $q->where('category_id', $choosedCategory );
            });
    	}

        $newses = $query->latest()->paginate(4);

        if (Request::ajax()) {
            return Response::json(View::make(Theme::path()->main .'.modules.newses.list_group', array('newses' => $newses))->render());
        }

    	return view(Theme::path()->main .'.modules.newses.list', compact('newses','categories'));
    }

    public function singleNews($id)
    {
        $news = News::find($id);

        $album = Album::find($news->album_id);

        return view(Theme::path()->main .'.modules.newses.single', compact('news','album'));
    }

    static function widget()
    {

        $newses = News::latest()->paginate(3);

        if (Request::ajax()) {
            return Response::json(View::make(Theme::path()->main .'.modules.newses.list_group_widget', array('newses' => $newses))->render());
        }

        View::share('newses',$newses);

    }

    private function getAllCategories()
    {
        $categories = NewsesCategory::all();

        return $categories;
    }



}
