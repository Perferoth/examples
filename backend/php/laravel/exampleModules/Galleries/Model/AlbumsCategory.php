<?php

namespace App\Modules\Galleries\Model;

use Illuminate\Database\Eloquent\Model;

class AlbumsCategory extends Model
{

    protected $table = 'albums_categories';
    protected $fillable = ['title','description','photoId','author','visible'];

    /**
     * Get the albums associated with the given categories.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function albums()
    {
        return $this->belongsToMany('App\Modules\Galleries\Model\Album','album_category','category_id','album_id');
    }
}
