<?php

namespace App\Modules\Galleries\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Album extends Model
{
    use SoftDeletes;
	protected $fillable = ['title','description','content','photos','author'];
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Get the categories associated with given Albums.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function categories()
    {
        return $this->belongsToMany('App\Modules\Galleries\Model\AlbumsCategory', 'album_category', 'album_id','category_id');
    }

    public function files()
    {
        return $this->belongsToMany('App\File', 'album_file','album_id','file_id');
    }
	
}
