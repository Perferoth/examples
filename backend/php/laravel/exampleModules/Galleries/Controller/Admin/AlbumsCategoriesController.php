<?php

namespace App\Modules\Galleries\Controller\Admin;

use App\Modules\Galleries\Model\AlbumsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Modules\Galleries\Model\Album;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Theme;


class AlbumsCategoriesController extends Controller
{
    public function listCategories()
    {
    	$categories = AlbumsCategory::all();

    	return view(Theme::path()->admin .'.modules.galleries.categories.list', compact('categories'));
    }

    public function formCategory($id = null)
    {
        if($id == NULL){
            $action = 'create';
        } else {
            $category = AlbumsCategory::find($id);
            $action = 'update';
        }

    	return view(Theme::path()->admin .'.modules.galleries.categories.form', compact('category','action','id'));
    }

    public function createCategory(Request $request)
    {
    	AlbumsCategory::create($request->all() +
    		[
    			'author' => Auth::user()->name
    		]);

    	$msgAdmin = 'Kategoria została dodana!';
    	return redirect()->to('office/galleries/albums/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);
    }

    public function updateCategory(Request $request,$id)
    {
        AlbumsCategory::find($id)->update($request->all() +
            [
                'author' => Auth::user()->name
            ]);

        $msgAdmin = 'Kategoria została zaktualizowana!';
        return redirect()->to('office/galleries/albums/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);

    }

    public function deleteCategory($id){

        AlbumsCategory::find($id)->delete();

        $msgAdmin = 'Kategoria została usunięta!';
        return redirect()->to('office/galleries/albums/categories')->with([
            'msgAdmin' => $msgAdmin,
            'status' => 'success'
        ]);

    }

}

