<?php
/**
 * @Date: 24.02.18 18:55
 * @Author: Piotr Ferfecki
 * @Company: FER-TECH Piotr Ferfecki
 *
 * @Function: Albums Controller
 */

namespace App\Modules\Galleries\Controller\Admin;

use App\Modules\Galleries\Model\AlbumsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Modules\Galleries\Model\Album;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Modules\Galleries\Controller\Admin\AlbumsCategoriesController;
use Yajra\Datatables\Datatables;
use Theme;
use App\Http\Controllers\Files\FilesController;


class AlbumsController extends Controller
{
    /**
     * Listing all albums in admin panel.
     * ---
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAlbums()
    {
    	$albums = Album::all();

    	return view(Theme::path()->admin .'.modules.galleries.albums.list', compact('albums'));
    }

    /**
     * Returning DataTables with albums data
     * @return mixed
     */
    public function getDatatableAlbumsList()
    {
        $albums = Album::query();

        return Datatables::of($albums)
            /*->editColumn('thumbnail', function ($products) {
                return self::fileThumbnail($products);
            })*/
            ->editColumn('actions', function ($albums) {
                return self::albumActions($albums, ['edit', 'delete']);
            })
            ->make(true);
    }

    private function albumActions($album, $setActions)
    {

        $actions = [
            'add' => '<a href="#add-' . $album->id . '" data-id="' . $album->id . '" data-action="add" class="action-button label label-success">Add</a>',
            'edit' => '<a href="albums/edit/' . $album->id . '" data-id="' . $album->id . '" data-action="edit" class="action-button label label-warning">Edytuj</a>',
            'delete' => '<a href="#delete-' . $album->id . '" data-id="' . $album->id . '" data-action="delete" class="action-button label label-danger">Usun</a>'
        ];

        $html = '';

        foreach ($setActions as $key) {
            $html .= $actions[$key];
            if ($key !== end($setActions)) {
                //$html .= '<span class="glyphicon glyphicon-option-vertical"></span>';
                $html .= '<span class="action-break-bar"></span>';
            }
        }

        return $html;
    }

    /**
     * Responsible for create/edit formular.
     * ---
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formAlbum($id = null)
    {
        if($id == NULL){
            $action = 'store';
        } else {
            $album = Album::find($id);

            $action = 'update';
        }

        $categories = AlbumsCategory::all();

    	return view(Theme::path()->admin .'.modules.galleries.albums.form', compact('album','action','id','categories'));
    }

    /**
     *  Responsible for saving record.
     *  ---
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createAlbum(Request $request)
    {
    	$album = Album::create($request->all() +
    		[
    			'author' => Auth::user()->name
    		]);

        if (!empty($request->filesId)) {
            $filesId = explode(',', $request->filesId);
            $this->bindFiles($filesId, $album);
        }

        $this->bindCategories($request->categories,$album);

    	return redirect()->to('office/galleries/albums');
    }

    public function storeFiles(Request $request)
    {
        $file = (new FilesController())->store($request);

        return $file->id;
    }

    /**
     *  Responsible for updating existing record.
     *  ---
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAlbum(Request $request, $id)
    {
        $album = Album::find($id);

        $album->update($request->all() +
            [
                'author' => Auth::user()->name
            ]);

        if (!empty($request->filesId)) {
            $filesId = explode(',', $request->filesId);
            $filesId = array_merge($album->files->pluck('id')->toArray(),$filesId);
            $this->bindFiles($filesId, $album);
        }

        $this->bindCategories($request->categories, $album);

        return redirect()->to('office/galleries/albums/edit/'. $album->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!empty($id)){
            if(Album::find($id)->delete()){
                $this->status = 'success';
                $this->msg = 'Album zostal skasowany!';
            } else {
                $this->status = 'error';
                $this->msg = 'Nie udalo sie skasowac albumu!';
            };
        } else {
            $this->status = 'error';
            $this->msg = 'Nie znaleziono ID albumu!';
        }

        $this->msg = View(Theme::path()->admin .'.components.messages')->with([
            'msg' => $this->msg,
            'status' => $this->status
        ])->render();

        return response()->json($this);
    }

    private function bindCategories($categoriesId , Album $album)
    {
            $album->categories()->sync((array) $categoriesId);
    }

    private function bindFiles($filesId , Album $album)
    {
        $album->files()->sync((array) $filesId);
    }



}

