<?php

namespace App\Modules\Albums\Controller\Frontend;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Modules\Albums\Model\Album;
use App\Modules\Albums\Model\AlbumsCategory;
use App\Http\Controllers\Controller;
use Theme;

class AlbumsController extends Controller
{
    public function showAlbums()
    {
    	$query = Album::query();

        $categories = $this->getAllCategories();

    	if(Request::input('category'))
        {
            $choosedCategory = Request::input('category');
            $query->whereHas('categories', function ($q) use ($choosedCategory) {
                $q->where('category_id', $choosedCategory );
            });
    	}

        $albums = $query->latest()->paginate(4);

        if (Request::ajax()) {
            return Response::json(View::make(Theme::path()->admin .'.modules.albums.list_group', array('albums' => $albums))->render());
        }

    	return view(Theme::path()->admin .'.modules.albums.list', compact('albums','categories'));
    }

    public function singleAlbum($id)
    {
        $album = Album::find($id);

        return view(Theme::path()->admin .'.modules.albums.single', compact('album'));
    }

    static function widget()
    {

        $albums = Album::latest()->paginate(5);

        if (Request::ajax()) {
            return Response::json(View::make(Theme::path()->admin .'.modules.albums.list_group_widget', array('albums' => $albums))->render());
        }

        View::share('albums', $albums);

    }

    private function getAllCategories()
    {
        $categories = AlbumsCategory::all();

        return $categories;
    }



}
